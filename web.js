require('newrelic');
var ack = require('ac-koa').require('hipchat');
var pkg = require('./package.json');
var Notifier = require('ac-koa-hipchat-notifier').Notifier;
var app = ack(pkg);

var addon = app.addon()
  .hipchat()
  .allowRoom(true)
  .scopes(['send_notification']);

if (process.env.DEV_KEY) {
  addon.key(process.env.DEV_KEY);
}

var notifier = Notifier({format: 'html', dir: __dirname + '/messages'});

// /pingbot -> show help message
addon.webhook('room_message', /^\/(pingbot)(\s)*(help)?$/, function *() {
    console.log("Log: Sending help message")
    yield notifier.sendTemplate('help');
});


// ping -> PONG
addon.webhook('room_message', /^\/?(ping)$/i, function () {
    console.log("Log: Just 1 nornal ping requested. Sending PONG.")
    this.roomClient.sendNotification('PONG', {notify: true, format: 'text'});
});


// ping N -> pong N times
addon.webhook('room_message', /^\/?ping\s(\d+)$/, function *() {
  var number = parseInt(this.match[1]);
  console.log("Log: Sending ping " + number + " times");
  for (var i = 1; i <= number; i++) {
      yield this.roomClient.sendNotification('pong ' + i, {notify: true, format: 'text'});
  }
});


/*
/ping me in 5 seconds OR /ping Loren 5 times OR /ping me with {text}
match[1] = /ping
match[2] = (\D*)? -- mentionName
match[3] = with
match [4] = (\C*) -- text to send if "with" is present
match[5] = (\d*)  -- number of times or seconds
match[6] = (second(s?)|time(s?))?
 */
addon.webhook('room_message', /(^\/ping)\s(@?\w*)?\s?(with\s(.*))?\D*(\d*)\s*(second(s?)|time(s?))?$/, function () {
  var roomClient = this.roomClient;
  var whatToMention = this.match[2] == null || this.match[2] == "me" ? this.sender.mention_name : this.match[2].replace("@","");
  var withExists = this.match[3] != null && this.match[3].contains("with");
  var textToSend = this.match[4];
  var numberOfSecondsOrTimes = parseInt(this.match[5]);
  var secondsOrTimesString = this.match[6] == null ? "seconds" : this.match[6];
  console.log("Log: whatToMention: " + whatToMention + ", withExists: " + withExists + ", textToSend: " + textToSend + ", numberOfTimesOrSeconds: " + numberOfSecondsOrTimes + ", secondsOrTimesString: " + secondsOrTimesString);
  var doCall = function (iteration, last_one) {
    var message = '@' + whatToMention + ' ';
    if (iteration) {
      message += iteration + ' time' + ((iteration > 1)?'s':'');
      if (last_one) {
        message = message + ' (ducreux)'
      }
    } else if (numberOfSecondsOrTimes) {
        message += whatToMention + ' it\'s been ' + numberOfSecondsOrTimes + ' seconds';
    } else if (withExists) {
        console.log("Log: Sending text: " + textToSend);
        message += textToSend;
    }
    roomClient.sendNotification(message, {notify: true, format: 'text'})
  };

  if(secondsOrTimesString.contains("time")) {
      console.log("Log: Sending mention " + numberOfSecondsOrTimes + " times");
    for (var i = 1; i <= numberOfSecondsOrTimes; i++) {
      setTimeout(doCall, (i-1)*1000, i, i == numberOfSecondsOrTimes);
    }
  } else {
    console.log("Log: Sending mention in " + numberOfSecondsOrTimes + " seconds");
    setTimeout(doCall, numberOfSecondsOrTimes*1000);
  }
});



/*
/ping me every 10 seconds for 60 seconds
match[1] = /ping
match[2] = (\D*)?   -- mentionName
match[4] = (\d*)    -- frequency
match[7] = (\d*)    -- total number of seconds
*/
addon.webhook('room_message', /(^\/ping)\s(@?\w*)?\s*(every)\s*(\d*)\s*(second(s?))\s*for\s*(\d*)\s*(second(s?))$/, function () {
    var roomClient = this.roomClient;
    var whatToMention = this.match[2] == null || this.match[2] == "me" ? this.sender.mention_name : this.match[2].replace("@","");
    var frequency = parseInt(this.match[4]);
    var totalTime = parseInt(this.match[7]);
    var frequencyFunction = function (index, total) {
      roomClient.sendNotification('@' + whatToMention + " " + index + " of " + total, {notify: true, format: 'text'})
    };
    var invocationCount = Math.floor(totalTime/frequency);

    console.log("Log: Sending mention " + invocationCount + " times (every " + frequency + " seconds for  " + totalTime + " seconds)");
    if(invocationCount != Infinity) {
        for (var i = 1; i <= invocationCount; i++) {
          setTimeout(frequencyFunction, frequency*i*1000, i, invocationCount);
        }
    } else {
        roomClient.sendNotification('(iseewhatyoudidthere)', {notify: true, format: 'text'});
    }
});



// /pingbot slash
addon.webhook('room_message', /(^\/pingbot)\s(\D*)?(slash)/, function () {
    var _this = this;
    console.log("Log: Sending slash commands");

    setTimeout(function() {
        _this.roomClient.sendNotification('testing message formatting slash commands:', {notify: true, format: 'text'});
    }, 0);
    setTimeout(function() {
        _this.roomClient.sendNotification('/code Testing /code and keyword highlighting: Public static void new (args[]) exit Chef ' +
            'require \'some green text here\' End Ran  [2015-03-05T12:33:31+00:00] ' +
            'for do in end var int float = 10; ' +
            'printf( "Value of float = %d\n", float);', {notify: true, format: 'text'});
    }, 1000);
    setTimeout(function() {
        _this.roomClient.sendNotification('/quote Testing /quote \n' +
            'Lilu Dallas multipass. (awesome) ', {notify: true, format: 'text'});
    }, 2000);
    setTimeout(function() {
        _this.roomClient.sendNotification('/me is testing /me (mariokart)', {notify: true, format: 'text'});
    }, 3000);
    setTimeout(function() {
        _this.roomClient.sendNotification('/em is testing /em (celeryman)', {notify: true, format: 'text'});
    }, 4000);
    setTimeout(function() {
        _this.roomClient.sendNotification('#205081 #3572b0', {notify: true, format: 'text'});
    }, 5000);

});


// /pingbot emoticons
addon.webhook('room_message', /(^\/pingbot)\s(\D*)?(emoticons|smilies|emojis)/, function () {
    console.log("Log: Sending emoticons");
    this.roomClient.sendNotification('Emoticons: (pbj) (content) (yey) (poo) (lol) (shipit) (christurkey) (ceilingcat) (potatodance) (mindblown) (selfhighfive) (evilburns) (grindsmygears) (troll) (invader)   (allthethings)', {notify: true, format: 'text'});
    this.roomClient.sendNotification('Smilies: :D :o :Z :p ;p >:-( :( 8) :\'( :# (embarrassed) O:) :-* (oops) :\\ :) :-| ;) (thumbsdown) (thumbsup) ', {notify: true, format: 'text'});
    this.roomClient.sendNotification('Emojis: (ಠᴥಠ)  👽 🐐 👀 👌 👙 🐮 🐼 🐳 🐙 🐩 🐢 🍄 🎉 🎶 📎 🍥 🍌 ⛵️ ➰ 〰 ♥️ ', {notify: true, format: 'text'});
});


// /pingbot languages
addon.webhook('room_message', /(^\/pingbot)\s(\D*)?(languages)/, function () {
    console.log("Log: Sending list of languages and special characters");
    // the following sample text is from http://www.columbia.edu/~kermit/utf8.html
    this.roomClient.sendNotification('English: The quick brown fox jumps over the lazy dog.\n\n' +
        'Irish: An ḃfuil ċroí bualaḋ ó ḟaitíos an ġrá a ṁeall ṗóg éada ó ṡlí do ṫú? D\'ḟuascail Íosa Úrṁac hÓiġe Beannaiṫe pór Éava agus Áḋaiṁ.\n\n' +
        'German: Im finſteren Jagdſchloß offenen Felsquellwaſſer affig-flatterhafte kauzig-höf‌liche Bäcker über\n\n' +
        'Norwegian: Blåbærsyltetøy\n\n' +
        'Swedish: Flygande bäckasiner söka strax hwila på mjuka tuvor.\n\n' +
        'Icelandic: Sævör grét áðan því úlpan var ónýt.\n\n' +
        'Polish: Pchnąć w tę łódź jeża lub osiem skrzyń fig.\n\n' +
        'Slovak: Starý kôň na hŕbe kníh žuje tíško povädnuté ruže, stĺpe sa ďateľ učí kvákať novú ódu živote.\n\n' +
        'Greek (monotonic): ξεσκεπάζω την ψυχοφθόρα βδελυγμία (polytonic): ξεσκεπάζω τὴν ψυχοφθόρα βδελυγμία\n\n' +
        'Russian: В чащах юга жил-был цитрус? Да, но фальшивый экземпляр! ёъ Съешь же ещё этих мягких французских булок да выпей чаю.\n\n' +
        'Bulgarian: Жълтата дюля беше щастлива, че пухът, който цъфна, замръзна като гьон.\n\n' +
        'Anglo-Saxon (Runes): ᛁᚳ᛫ᛗᚨᚷ᛫ᚷᛚᚨᛋ᛫ᛖᚩᛏᚪᚾ᛫ᚩᚾᛞ᛫ᚻᛁᛏ᛫ᚾᛖ᛫ᚻᛖᚪᚱᛗᛁᚪᚧ᛫ᛗᛖ᛬\n\n' +
        'Hungarian: Árvíztűrő tükörfúrógép.\n\n' +
        'English (IPA): [aɪ kæn iːt glɑːs ænd ɪt dɐz nɒt hɜːt miː] (Pronunciation)\n\n' +
        'Hebrew: זה כיף סתם לשמוע איך תנצח קרפד עץ טוב בגן.\n' +
        'Japanese: いろはにほへど　ちりぬるを わがよたれぞ　つねならむ うゐのおくやま　けふこえて あさきゆめみじ　ゑひもせず\n\n' +
        'Chinese (Trad.): 我能吞下玻璃而不傷身體。\n\n' +
        'Arabic: أنا قادر على أكل الزجاج و هذا لا يؤلمني.\n\n ' +
        'Georgian: მინას ვჭამ და არა მტკივა.\n\n' +
        'Armenian: Կրնամ ապակի ուտել և ինծի անհանգիստ չըներ։\n\n' +
        'Hindi: मैं काँच खा सकता हूँ और मुझे उससे कोई चोट नहीं पहुंचती.\n\n' +
        'Burmese: က္ယ္ဝန္‌တော္‌၊က္ယ္ဝန္‌မ မ္ယက္‌စားနုိင္‌သည္‌။ ၎က္ရောင္‌့ ထိခုိက္‌မ္ဟု မရ္ဟိပ\n\n' +
        'Korean: 나는 유리를 먹을 수 있어요. 그래도 아프지 않아요\n\n' +
        'Navajo: Tsésǫʼ yishą́ągo bííníshghah dóó doo shił neezgai da.\n\n ' +
        'Inuktitut: ᐊᓕᒍᖅ ᓂᕆᔭᕌᖓᒃᑯ ᓱᕋᙱᑦᑐᓐᓇᖅᑐᖓ\n\n' +
        'Random characters:   ¥ · £ · € · $ · ¢ · ₡ · ₢ · ₣ · ₤ · ₥ · ₦ · ₧ · ₨ · ₩ · ₪ · ₫ · ₭ · ₮ · ₯ · ₹\n\n' +
        'And now some Zalgo: h̤̯i̤̬,̣̥̦͖ ̙̲̟m̧̭̗͖̩͔͈ͅy҉̥͙̗͙̤͎ ͅn͝a̜̣͟me̷̞ ̨͎̬͎͉͚i̵̼͈͚̫s̱̫͉͇ ̀Ĺ̤̠͙̪̟͉o̴̙̳̙͇̗̲͉r̸e͍̜n͏̩͓̭̠ ̴̩̺͚̺͍̗M̲̺̺͚̭͉͈ị̰̫͖̤͖c̯̖̙̰͓h͉̙̬̣ḙ̵̞͙̟ͅl̶̜̦͉ͅo͏̣̯͕͍̯̭̲n͓̖̬̰i̗̠̪̠̙.̯̹̳ ̴̖̤̞̳W͠h͇̗͝a̛͉̩̞̭͙t̘͖͜s̯̝͝ ͈̭̘u̗̬͚͓͚͉͘ͅp̧̯̝̬͔͕̤ͅ ̻͔̭̝̩͞yo', {notify: true, format: 'text'});
});




addon.webhook('room_message', /(^\/pingbot)\s(\D*)?(send me some lists)/, function () {
    console.log("Log: Sending some lists");
    this.roomClient.sendNotification('Unordered list with a lot of nesting\n' +
        '<ul>' +
        '    <li>one' +
        '        <ul>' +
        '        <li>one.one' +
        '            <ul>' +
        '            <li>one.one.one' +
        '                <ul>' +
        '                <li>one.one.one.one' +
        '                    <ul>' +
        '                    <li>1.1.1.1.1</li>' +
        '                    <li>1.1.1.1.2</li>' +
        '                    </ul>' +
        '               </li>' +
        '               </ul>' +
        '            </li>' +
        '            </ul>' +
        '        </li>' +
        '        </ul>' +
        '    </li>' +
        '    <li>Two' +
        '        <ul>' +
        '        <li>two.one</li> ' +
        '        <li>two.two</li>' +
        '        </ul>' +
        '    </li>' +
        '    <li>Three</li>' +
        '</ul>', {notify: true, format: 'html'});


    this.roomClient.sendNotification('Unordered list with a lot of unordered nesting \n' +
        '<ol>' +
        '<li>Milk' +
        '    <ol>' +
        '    <li>Goat</li>' +
        '    <li>Cow</li>' +
        '    </ol>' +
        '</li>' +
        '<li>Eggs' +
        '    <ol>' +
        '    <li>Free-range</li>' +
        '    <li>Other</li>' +
        '    </ol>' +
        '</li>' +
        '<li>Cheese' +
        '    <ol>' +
        '    <li>American' +
        '        <ol>' +
        '        <li>real</li>' +
        '        <li>fake</li>' +
        '        </ol>' +
        '    </li>' +
        '    <li>French' +
        '        <ol>' +
        '        <li>Smelly</li>' +
        '        <li>Extra smelly' +
        '            <ol>' +
        '            <li>Pont l’Eveque</li>' +
        '            <li>Camembert de Normandy' +
        '                <ol>' +
        '                <li>pasturized milk</li>' +
        '                <li>raw milk</li>' +
        '                </ol>' +
        '            </li>' +
        '            </ol>' +
        '        </li>' +
        '       </ol>' +
        '    </li>' +
        '    </ol>' +
        ' </li>' +
        '</ol>', {notify: true, format: 'html'});

});

addon.webhook('room_message', /(^\/pingbot)\s(\D*)?(send me some html)/, function () {
    console.log("Sending HTML");
    this.roomClient.sendNotification('Sending some HTML <br>' +
        '<a href="https://www.google.com">This is a link</a><br>' +
        '<b>This is bold text</b><br>' +
        '<strong>This is strong text</strong><br>' +
        '<i>This text is italicized</i><br>' +
        '<em>This is emphasized text</em><br>' +
        '<code>This is some sample code</code><br>' +
        'Testing span, heading, &amp; color: <h1>My <span style="color:red">Important</span> Heading</h1><br>' +
        'Testing &lt;br&gt;<br>first line <br>second line <br>' +
        '<pre>This is text in a <b>pre</b> element. It\'s displayed in a fixed font and preserves both        spaces         and line\nbre<br>aks</pre><br>' +
        'Here\'s a list: <ul><li>Coffee</li><li>Tea</li><li>Milk</li></ul> <br>' +
        'Here\'s a table: <table border="2">' +
        '       <tr>' +
        '        <td>Jill</td>' +
        '        <td>Smith</td>' +
        '        <td>50</td>' +
        '        </tr>' +
        '        <tr>' +
        '        <td>Eve</td>' +
        '        <td>Jackson</td>' +
        '        <td>94</td>' +
        '        </tr>' +
        '        <tr>' +
        '        <td>John</td>' +
        '        <td>Doe</td>' +
        '        <td>80</td>' +
        '        </tr>' +
        '        </table> ' +
        'Image with a specified size <img src="http://media.giphy.com/media/5xtDaruSlTqZOdKjmVO/giphy.gif" alt="some_text" style="width:75px;height:75px;"> Image without a specified size <img src="http://media.giphy.com/media/5xtDaruSlTqZOdKjmVO/giphy.gif" alt="some_text"> ', {notify: true, format: 'html'});
    this.roomClient.sendNotification('This message is green. <a href="https://en.wikipedia.org/wiki/Green">And here is a link that should be green</a>', {color: 'green', notify: true, format: 'html'});
    this.roomClient.sendNotification('This message is red. <a href="https://en.wikipedia.org/wiki/Red">And here is a link that should be red</a>', {color: 'red', notify: true, format: 'html'});
    this.roomClient.sendNotification('This message is purple (Purple? Yes, Purple). <a href="https://en.wikipedia.org/wiki/Purple">And here is a link that should be purple</a>', {color: 'purple', notify: true, format: 'html'});
    this.roomClient.sendNotification('This message is yellow. <a href="https://en.wikipedia.org/wiki/Yellow">And here is a link that should be yellow</a>', {color: 'yellow', notify: true, format: 'html'});
    this.roomClient.sendNotification('This message is gray. <a href="https://en.wikipedia.org/wiki/Gray">And here is a link that should be gray</a>', {color: 'gray', notify: true, format: 'html'});

});

app.listen();
