# What is this?

A simple HipChat add-on that can be used as a testing tool for HipChat. The add-on sends messages into a room as a notification with certain commands.


# Usage:  In general use /ping for mentions and ping for pongs
|  |  |
| ------------- | ------------------------------ |
| `/pingbot` |  print this help message   |
| `ping` | sends 'pong' message |
| `ping N` | sends 'pong' message N times |
| `/ping me` | sends message with @{your mention name}|
| `/ping {user} ` | sends message with @{user}  |
| `/ping {user} in N` | sends message with @{user} in N seconds|
| `/ping {user} N times` | sends message with @{user} N times |
| `/ping {user} with {text}`  | sends message with @{user} {text}|
| `/ping {user} every {x} seconds for {y} seconds` | sends message with @{user} every {x} seconds for {y} seconds |
| `/pingbot emoticons` | sends a set of emoticons, smilies, and emojis|
| `/pingbot slash` | sends a set of message formatting slash commands|
| `/pingbot languages`	| sends text in different languages that have special characters
| `/pingbot send me some lists`	| sends a bunch of nested lists in html

# Other information about the add-on

I started with the [ac-koa-hipchat-vagrant](https://bitbucket.org/atlassianlabs/ac-koa-hipchat-vagrant) template for this add-on. It's written in JS and is currently running on heroku at https://hc-pingbot.herokuapp.com/addon/capabilities


# How do I get it?

Click https://hipchat.com/addons/install?url=https://hc-pingbot.herokuapp.com/addon/capabilities and select the room where you want to use Pingbot
